<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Donatur extends Model
{
    // use HasFactory;
    protected $table = 'donatur';
    protected $fillable = ['id', 'nama', 'alamat', 'pekerjaan', 'no_telp', 'no_transaksi', 'role'];
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OfficeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = [
            [
                'nama_kantor' => 'Human Initiative Pusat',
            ],
            [
                'nama_kantor' => 'Human Initiative Riau',
            ],
            [
                'nama_kantor' => 'Human Initiative Jawa Timur',
            ]
        ];
        DB::table('role_office')->insert($post);
    }
}

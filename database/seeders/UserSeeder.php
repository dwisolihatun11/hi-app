<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 
        $post = [
            [
                'nama' => 'Admin Pusat',
                'username' => 'adminpusat',
                'password' => Hash::make('password'),
                'role' => 1
            ],
            [
                'nama' => 'Admin Riau',
                'username' => 'cabangriau',
                'password' => Hash::make('password'),
                'role' => 2
            ],
            [
                'nama' => 'Admin Jawa Timur',
                'username' => 'cabangjatim',
                'password' => Hash::make('password'),
                'role' => 3
            ]
        ];
        DB::table('user')->insert($post);
    }
}
